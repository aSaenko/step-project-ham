const tabsCaption = document.querySelectorAll('.tabs li');
for (let i = 0; i < tabsCaption.length; i++) {
    tabsCaption[i].addEventListener('click', function () {
        this.classList.add('active');
        let sibling = this.previousElementSibling;
        while (sibling) {
            sibling.classList.remove('active');
            sibling = sibling.previousElementSibling;
        }
        sibling = this.nextElementSibling;
        while (sibling) {
            sibling.classList.remove('active');
            sibling = sibling.nextElementSibling
        }
        const tabContentId = this.dataset.tabs;
        const tabContent = document.getElementById(tabContentId);
        const tabsContainer = tabContent.parentElement;

        const allTabsContent = tabsContainer.children;
        for (let i = 0; i < allTabsContent.length; i++) {
            allTabsContent[i].classList.remove('active');
        }
        tabContent.classList.add('active');
    })
}


$(".amazing-work-tabs li").click(function () {
    $(this).addClass('active').siblings().removeClass('active');

    const tabContentId = $(this).data('tabs');
    $(`#${tabContentId}`).addClass('active');
    $(`#${tabContentId}`).siblings().removeClass('active');
});

$('.load-more').click(function () {
    if((this).length === 0){
        $(this).addClass('active');
    }
    else {
        $(this).remove()
    }

   const tabContentId = $(this).data('tabs');
    $(`#${tabContentId}`).addClass('active');
    console.log(tabContentId);
});



$(document).ready(function(){
    $('.slider-photo-user').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-photo-user-bottom',

    });
    $('.slider-photo-user-bottom').slick({
        initialSlide: 1,
        infinite: true,
        centerMode: true,
        slidesToScroll: 1,
        focusOnSelect: true,
        asNavFor: '.slider-photo-user',
        dots: true,
        variableWidth: true,
        draggable: false,
        cssEase: 'linear',
        speed: 550,
        prevArrow: document.querySelector('.prev-btn'),
        nextArrow: document.querySelector('.next-btn'),




    });
});

// $(document).ready(function(){
//     $('.slider-photo-user').slick({
//         slidesToShow: 1,
//         slidesToScroll: 1,
//         initialSlide: 1,
//         arrows: false,
//         fade: true,
//         asNavFor: '.slider-photo-user-bottom',
//
//     });
//     $('.slider-photo-user-bottom').slick({
//         slidesToShow:1,
//         slidesToScroll: 1,
//         asNavFor: '.slider-photo-user',
//         dots: true,
//         centerMode: true,
//         focusOnSelect: true,
//
//
//     });
// });





